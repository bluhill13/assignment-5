﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_5.Exceptions
{
    public class ArrayFullException : Exception
    {
        public int Size { get; set; }
        public ArrayFullException()
        {

        }
        public ArrayFullException(int size) : base (String.Format($"Array is full with {size} elements!"))
        {
            Size = size;
        }
    }
}
