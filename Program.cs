﻿using Assignment_5.Animals;
using Assignment_5.Animals.BirdTypes;
using Assignment_5.Animals.FishTypes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

namespace Assignment_5
{
    class Program
    {
        public static List<Animal> animals = new List<Animal>();
        public static int FilterWeight = 0;
        public static int FilterLenght = 0;
        static void Main(string[] args)
        {

            animals = HelperClass.GenerateAnimals();
            do
            {
                Console.WriteLine("\n\tThe life of fish");
                Console.WriteLine("\nMenu");
                Console.WriteLine("Write '1' to see a fish type, '2' to see a bird type, 3 to feed a Shark, 4 to feed an eagle or write 'exit' to exit program");
                Console.Write("Command: ");
                string input = Console.ReadLine();
                if (input == "exit") break;
                Menu(input);
            } while (true);

        }
        /// <summary>
        /// Guides the user to making a choice and goes to default in switch if an invalid number have been entered
        /// </summary>
        /// <param name="input"></param>
        public static void Menu(string input)
        {
            try
            {
                int choice = Int32.Parse(input);
                switch (choice)
                {
                    case 1:
                        Console.WriteLine("Write '1' to get all Salmon's, '2' to get all Trout's, '3' to get all Shark's");
                        Console.Write("Command");
                        string fish = Console.ReadLine();
                        GetFishes(fish);
                        break;
                    case 2:
                        Console.WriteLine("Write '1' to get all Eagles, '2' to get all Seagull's, '3' to get all Albatrosse's");
                        Console.Write("Command");
                        string bird = Console.ReadLine();
                        GetBirds(bird);
                        break;
                    case 3:
                        GetFishes("3");
                        Console.WriteLine("Select a shark id to feed it");
                        string sharkId = Console.ReadLine();
                        FeedTheShark(sharkId);
                        break;
                    case 4:
                        GetBirds("1");
                        Console.WriteLine("Select a Eagle id to feed it a fish");
                        string eagleId = Console.ReadLine();
                        FeedTheEagle(eagleId);
                        break;
                    default:
                        //error message
                        Console.WriteLine("You have entered an invalid choice, try again");
                        break;
                };
            }
            catch (FormatException) { Console.WriteLine("Something went wrong with the input you gave try a valid number"); }
        }
        
        /// <summary>
        /// Prints all nessesary information about a fish type selected by the user
        /// Will give error message if it is an invalid choice
        /// </summary>
        /// <param name="input"></param>
        public static void GetFishes(string input)
        {
            int choice = Int32.Parse(input);
            switch (choice)
            {
                case 1:
                    IEnumerable<Salmon> salmon;
                    if (HelperClass.Filter(true)) salmon = animals.OfType<Salmon>().Where(o => o.Weight > FilterWeight).OrderBy(s => s.Weight);
                    else salmon = animals.OfType<Salmon>().Where(o => o.Lenght > FilterLenght).OrderBy(s => s.Lenght > FilterLenght);
                    if (salmon.Count() == 0) { Console.WriteLine("Found nothing!"); break; }
                    foreach (Salmon i in salmon)
                    {
                        i.PrintAllInfo();
                    }
                    break;
                case 2:
                    IEnumerable<Trout> temp;
                    if (HelperClass.Filter(true)) temp = animals.OfType<Trout>().Where(o => o.Weight > FilterWeight).OrderBy(s => s.Weight);
                    else temp = animals.OfType<Trout>().Where(o => o.Lenght > FilterLenght).OrderBy(s => s.Lenght > FilterLenght);
                    if (temp.Count() == 0) { Console.WriteLine("Found nothing!"); break; }
                    foreach (Trout i in temp)
                    {
                        i.PrintAllInfo();
                    }
                    break;
                case 3:
                    IEnumerable<Shark> shark;
                    if (HelperClass.Filter(true)) shark = animals.OfType<Shark>().Where(o => o.Weight > FilterWeight).OrderBy(s => s.Weight);
                    else shark = animals.OfType<Shark>().Where(o => o.Lenght > FilterLenght).OrderBy(s => s.Lenght > FilterLenght);
                    if (shark.Count() == 0) { Console.WriteLine("Found nothing!"); break; }
                    foreach (Shark i in shark)
                    {
                        i.PrintAllInfo();
                    }
                    break;
                default:
                    Console.WriteLine("You have entered an invalid choice, start again");
                    break;
            }
        }
        /// <summary>
        /// Prints all nessesary information about a bird type selected by the user
        /// Will give error message if it is an invalid choice
        /// </summary>
        /// <param name="input"></param>
        public static void GetBirds(string input)
        {
            int choice = Int32.Parse(input);
            switch (choice)
            {
                case 1:
                    IEnumerable<Eagle> eagle;
                    if (HelperClass.Filter(false)) eagle = animals.OfType<Eagle>().Where(o => o.Weight > FilterWeight).OrderBy(s => s.Weight);
                    else eagle = animals.OfType<Eagle>().Where(o => o.Lenght > FilterLenght).OrderBy(s => s.Lenght > FilterLenght);
                    if (eagle.Count() == 0) { Console.WriteLine("Found nothing!"); break; }
                    foreach (Eagle i in eagle)
                    {
                        i.PrintAllInfo();
                    }
                    break;
                case 2:
                    IEnumerable<Seagul> seagull;
                    if (HelperClass.Filter(false)) seagull = animals.OfType<Seagul>().Where(o => o.Weight > FilterWeight).OrderBy(s => s.Weight);
                    else seagull = animals.OfType<Seagul>().Where(o => o.Lenght > FilterLenght).OrderBy(s => s.Lenght > FilterLenght);
                    if (seagull.Count() == 0) { Console.WriteLine("Found nothing!"); break; }
                    foreach (Seagul i in seagull)
                    {
                        i.PrintAllInfo();
                    }
                    break;
                case 3:
                    IEnumerable<Albatross> albatross;
                    if (HelperClass.Filter(false)) albatross = animals.OfType<Albatross>().Where(o => o.Weight > FilterWeight).OrderBy(s => s.Weight);
                    else albatross = animals.OfType<Albatross>().Where(o => o.Lenght > FilterLenght).OrderBy(s => s.Lenght > FilterLenght);
                    if (albatross.Count() == 0) { Console.WriteLine("Found nothing!"); break; }
                    foreach (Albatross i in albatross)
                    {
                        i.PrintAllInfo();
                    }
                    break;
                default:
                    Console.WriteLine("You have entered an invalid choice, start again");
                    break;
            }
        }
        /// <summary>
        /// Feed a shark with a id provided by the user or selects a shark at random to feed it a salomon
        /// </summary>
        /// <param name="sharkId"></param>
        public static void FeedTheShark(string sharkId)
        {
            try
            {
                int id = Int32.Parse(sharkId);
                Shark s = animals.OfType<Shark>().Where(o => o.Id == id).FirstOrDefault();
                s.Eat("Random animal");
                //Get random animal to eat
                IEnumerable<Salmon> eaten = animals.OfType<Salmon>().Where(o => o.IsDead = true);
                int test = HelperClass.getRandomNumber(eaten.Count());
                eaten.ElementAt(HelperClass.getRandomNumber(eaten.Count())).Die();
            }
            catch (FormatException)
            {
                Shark s = animals.OfType<Shark>().FirstOrDefault();
                s.Eat("Random animal");
                IEnumerable<Salmon> eaten = animals.OfType<Salmon>().Where(o => o.IsDead = true);
                int test = HelperClass.getRandomNumber(eaten.Count());
                eaten.ElementAt(HelperClass.getRandomNumber(eaten.Count())).Die();
            }
        }
        /// <summary>
        /// Feed a eagle with a id provided by user or selects one at random to feed it a seagull
        /// </summary>
        /// <param name="eagleId"></param>
        public static void FeedTheEagle(string eagleId)
        {
            try
            {
                int id = Int32.Parse(eagleId);
                Eagle s = animals.OfType<Eagle>().Where(o => o.Id == id).FirstOrDefault();
                s.Eat("Random animal");
                //Get random animal to eat
                IEnumerable<Salmon> eaten = animals.OfType<Salmon>().Where(o => o.IsDead = true);
                int test = HelperClass.getRandomNumber(eaten.Count());
                eaten.ElementAt(HelperClass.getRandomNumber(eaten.Count())).Die();

            }
            catch (FormatException)
            {
                Shark s = animals.OfType<Shark>().FirstOrDefault();
                s.Eat("Random animal");
                //Get random animal to eat
                IEnumerable<Salmon> eaten = animals.OfType<Salmon>().Where(o => o.IsDead = true);
                int test = HelperClass.getRandomNumber(eaten.Count());
                eaten.ElementAt(HelperClass.getRandomNumber(eaten.Count())).Die();
            }
        }


    }
}
