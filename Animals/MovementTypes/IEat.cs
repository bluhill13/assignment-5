﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_5.Animals.MovementTypes
{
    interface IEat
    {
        void EatingInfo();
        void Eat(string food);
    }
}
