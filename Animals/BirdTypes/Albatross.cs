﻿using Assignment_5.Animals.MovementTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_5.Animals.BirdTypes
{
    public class Albatross : Bird, IFly
    {
        public Albatross(string name)
        {
            AnimalName = name;
        }

        public Albatross(string name, int id, int lenght, int weight, bool isDead = false)
        {
            AnimalName = name;
            Lenght = lenght;
            Id = id;
            Weight = weight;
            IsDead = isDead;
        }
        public override void Eat(string food)
        {
            Console.WriteLine($"*Eats {food}*");
            NumberOfFishEaten += 1;
            Console.WriteLine($"The {AnimalName} have now consumed a total of {NumberOfFishEaten} fish");
        }

        public override void EatingInfo()
        {
            Console.WriteLine("The albatross eats cephalopods, fish, crustaceans and offal. Sometimes, " +
                "they may also eat carrion and or other kinds of zooplankton. However, the importance " +
                "of each food is different according to each species. Some may like to eat only squids, " +
                "or others might eat more krill or fish.");
        }

        public void Fly()
        {
            Console.WriteLine("*Flap flap, glide*");
        }

        public override void PrintAllInfo()
        {
            Console.WriteLine($"The {AnimalName} with id: {Id} have a wingspan of {Lenght} cm and weighs {Weight / 10} kg");
            Console.WriteLine($"So far in its life it has eaten {NumberOfFishEaten}");
        }

        public override void Screech()
        {
            Console.WriteLine("Screeeeaaaach!");
        }
    }
}
