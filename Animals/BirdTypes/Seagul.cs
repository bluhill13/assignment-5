﻿using Assignment_5.Animals.MovementTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_5.Animals.BirdTypes
{
    public class Seagul : Bird, IFly
    {
        public Seagul(string name)
        {
            AnimalName = name;
        }

        public Seagul(string name, int id, int lenght, int weight, bool isDead = false)
        {
            AnimalName = name;
            Lenght = lenght;
            Id = id;
            Weight = weight;
            IsDead = isDead;
        }
        public override void Eat(string food)
        {
            Console.WriteLine($"*Eats {food}*");
            NumberOfFishEaten += 1;
            Console.WriteLine($"The {AnimalName} have now consumed a total of {NumberOfFishEaten} fish");
        }

        public void Die()
        {
            Console.WriteLine("*Seagull dies*");
        }

        public override void EatingInfo()
        {
            Console.WriteLine("The food taken by gulls includes fish and marine and freshwater invertebrates, " +
                "both alive and already dead, terrestrial arthropods and invertebrates such as insects and earthworms," +
                " rodents, eggs, carrion, offal, reptiles, amphibians, plant items such as seeds and fruit, " +
                "human refuse, chips, and even other birds.");
        }

        public override void PrintAllInfo()
        {
            Console.WriteLine($"The {AnimalName} with id: {Id} have a wingspan of {Lenght} cm and weighs {Weight / 10} kg");
            Console.WriteLine($"So far in its life it has eaten {NumberOfFishEaten} fish");
        }

        public override void Screech()
        {
            Console.WriteLine("*Ubelivable annoying sound*");
        }

        public void Fly()
        {
            Console.WriteLine("*Flap flap, glide*");
        }
    }
}
