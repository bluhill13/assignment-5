﻿using Assignment_5.Animals.MovementTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_5.Animals.BirdTypes
{
    public class Eagle : Bird, IFly
    {
        public Eagle(string name)
        {
            AnimalName = name;
        }

        public Eagle(string name, int id, int lenght, int weight, bool isDead = false)
        {
            AnimalName = name;
            Id = id;
            Lenght = lenght;
            Weight = weight;
            IsDead = isDead;
        }

        public int NumberOfBirdsEaten { get; set; }
        public override void Eat(string food)
        {
            Console.WriteLine($"*Eats {food}*");
            NumberOfFishEaten += 1;
            Console.WriteLine($"The {AnimalName} have now consumed a total of {NumberOfFishEaten} fish and {NumberOfBirdsEaten} birds");
        }

        public override void EatingInfo()
        {
            Console.WriteLine("Their prey items include waterfowl and small mammals like squirrels, prairie dogs," +
                " raccoons and rabbits. Bald eagles are opportunistic predators meaning that in addition to hunting" +
                " for live prey, they will steal from other animals (primarily from other eagles or smaller fish eating birds)" +
                " or scavenge on carrion.");
        }

        public void Fly()
        {
            Console.WriteLine("*Flap flap, glide*");
        }

        public override void PrintAllInfo()
        {
            Console.WriteLine($"The {AnimalName} with id: {Id} have a wingspan of {Lenght} cm and weighs {Weight / 10} kg");
            Console.WriteLine($"So far in its life it has eaten {NumberOfFishEaten} fish and {NumberOfBirdsEaten} birds");
        }

        public override void Screech()
        {
            Console.WriteLine("Screeeeeech!!!!!");
        }
    }
}
