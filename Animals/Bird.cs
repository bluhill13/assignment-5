﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_5.Animals
{
    public abstract class Bird : Animal
    {
        public bool CanFly { get; set; }
        public int NumberOfFishEaten { get; set; }

        public abstract void Screech();

        public void Die()
        {
            IsDead = true;
            Console.WriteLine($"*{AnimalName} dies*");
            Console.WriteLine($"The {AnimalName} died of natural causes, it had the wingspan of {Lenght} cm and weight of {Weight / 10} kg");
           
        }

        public void PrintName(int id)
        {
            Console.WriteLine($"Bird id: {id}, Bird name: {AnimalName}");
        }
    }
}
