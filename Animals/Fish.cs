﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_5.Animals
{
    public abstract class Fish : Animal
    {
        
        public bool FreshWater { get; set; }
        
        public void Die()
        {
            IsDead = true;
            Console.WriteLine($"*{AnimalName} dies*");
            if (FreshWater) Console.WriteLine($"The {AnimalName} died of natural causes in freshwater, it had the lenght of {Lenght} cm and weight of {Weight / 10} kg");
            else Console.WriteLine($"The {AnimalName} died of natural causes in the sea, it had the lenght of {Lenght} cm and weight of {Weight / 10} kg");
        }

        public void PrintName(int id)
        {
            Console.WriteLine($"Fish id: {id}, Fish name: {AnimalName}");
        }

        
    }
}
