﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_5.Animals.FishTypes
{
    public class Trout : Fish
    {
        public Trout()
        {
            AnimalName = "Trout";
        }
        public Trout(string name, int id, int lenght, int weight, bool freshWater, bool isDead = false)
        {
            AnimalName = name;
            Lenght = lenght;
            Id = id;
            Weight = weight;
            FreshWater = freshWater;
            IsDead = isDead;
        }

        public override void Eat(string food)
        {
            Console.WriteLine($"*Eats {food}*");
        }

        public override void EatingInfo()
        {
            Console.WriteLine("Trout eat a host of aquatic insects, terrestrial insects, " +
                "other fish, crustaceans, leeches, worms, and other foods. The food items that " +
                "are most important to trout and fly fishers are the aquatic insects that spend" +
                " most of their life cycles underwater in rivers, streams, and stillwaters");
        }

        public override void PrintAllInfo()
        {
            Console.WriteLine($"The {AnimalName} with id: {Id} is {Lenght} cm long, weighs {Weight / 10} kg");
            if (IsDead) Console.WriteLine("It is also still alive!");
            else Console.WriteLine("The fish is dead :(");
        }
    }
}
