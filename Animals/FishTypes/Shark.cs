﻿using Assignment_5.Animals.MovementTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_5.Animals.FishTypes
{
    public class Shark : Fish, ISwim
    {
        public int NumberOfFishEaten { get; set; }
        public Shark()
        {
            AnimalName = "Shark";
        }
        public Shark(string name, int id, int lenght, int weight, bool freshWater, bool isDead = false)
        {
            AnimalName = name;
            Lenght = lenght;
            Id = id;
            Weight = weight;
            FreshWater = freshWater;
            IsDead = isDead;
        }
        public override void Eat(string food)
        {
            Console.WriteLine($"*Eats {food}*");
            NumberOfFishEaten += 1;
            Console.WriteLine($"The shark have now consumed {NumberOfFishEaten} fish");
        }

        public override void EatingInfo()
        {
            Console.WriteLine("Sharks are opportunistic feeders, but most sharks primarily feed on smaller fish and invertebrates. " +
                "Some of the larger shark species prey on seals, sea lions, and other marine mammals. Sharks have been known to " +
                "attack humans when they are confused or curious.");
        }

        public override void PrintAllInfo()
        {
            Console.WriteLine($"The {AnimalName} with id: {Id} is {Lenght} cm long, weighs {Weight / 10} kg");
            if (IsDead) Console.WriteLine($"It is also still alive! and has eaten {NumberOfFishEaten} so far in its life");
            else Console.WriteLine($"The fish is dead :( and i managed to eat {NumberOfFishEaten} before it died");
        }

        public void Swim()
        {
            if (FreshWater) Console.WriteLine("*Swims in freshwater*");
            else Console.WriteLine("*Swims in freshwater*");
        }
    }
}
