﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_5.Animals.FishTypes
{
    public class Salmon : Fish
    {
        public Salmon()
        {
            AnimalName = "Salmon";
        }

        public Salmon(string name, int id, int lenght, int weight, bool freshWater, bool isDead = false)
        {
            AnimalName = name;
            Lenght = lenght;
            Id = id;
            Weight = weight;
            FreshWater = freshWater;
            IsDead = isDead;
        }
        public override void Eat(string food)
        {
            Console.WriteLine($"*Eats {food}*");
        }

        public override void EatingInfo()
        {
            Console.WriteLine("In the wild, salmon may dine on zooplankton and small invertebrates. " +
                "Once they get a tad bigger, salmon can readily eat smaller fish, like herring, or the " +
                "shrimp-like critter called krill. Salmon kept in farms are usually fed a ground-up mixture " +
                "of other fish and organisms from the ocean.");
        }

        public override void PrintAllInfo()
        {
            Console.WriteLine($"The {AnimalName} with id: {Id} is {Lenght} cm long, weighs {Weight / 10} kg");
            if (IsDead) Console.WriteLine("It is also still alive!");
            else Console.WriteLine("The fish is dead :(");
        }
    }
}
