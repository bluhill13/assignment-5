﻿using Assignment_5.Animals.MovementTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Assignment_5.Animals
{
    public abstract class Animal : IEat
    {
        public string AnimalName { get; set; }
        public string Name { get; set; }
        public int Id { get; set; }
        public int Lenght { get; set; }
        public int Weight { get; set; }
        public bool IsDead { get; set; }
        public abstract void EatingInfo();
        public abstract void Eat(string food);
        public abstract void PrintAllInfo();
    }
}
