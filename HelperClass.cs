﻿using Assignment_5.Animals;
using Assignment_5.Animals.BirdTypes;
using Assignment_5.Animals.FishTypes;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Assignment_5
{
    public class HelperClass
    {
        /// <summary>
        /// Creates all the animals and return the list containing them all
        /// </summary>
        /// <returns></returns>
        public static List<Animal> GenerateAnimals()
        {
            List<Animal> animals = new List<Animal>();
            HelperClass.CreateAnimal(5, 1, ref animals);
            HelperClass.CreateAnimal(5, 2, ref animals);
            HelperClass.CreateAnimal(5, 3, ref animals);
            HelperClass.CreateAnimal(5, 4, ref animals);
            HelperClass.CreateAnimal(5, 5, ref animals);
            HelperClass.CreateAnimal(5, 6, ref animals);
            return animals;
        }
        /// <summary>
        /// Creates animals based on user choice
        /// uses randomnumber generator to give random features to object
        /// </summary>
        /// <param name="number"></param>
        /// <param name="moreInfo"></param>
        /// <returns></returns>
        public static void CreateAnimal(int number, int birdtype, ref List<Animal> data)
        {
            for (int i = 0; i < number; i++)
            {
                switch (birdtype)
                {
                    case 1:
                        Eagle eagle = new Eagle("Eagle", data.Count, getRandomNumber(300), getRandomNumber(3000), true); data.Add(eagle);
                        break;
                    case 2:
                        Seagul seagul = new Seagul("Seagull", data.Count, getRandomNumber(100), getRandomNumber(1000), true); data.Add(seagul);
                        break;
                    case 3:
                        Albatross albatros = new Albatross("Albatross", data.Count, getRandomNumber(400), getRandomNumber(4000), true); data.Add(albatros);
                        break;
                    case 4:
                        Salmon salmon = new Salmon("Salmon", data.Count, getRandomNumber(300), getRandomNumber(3000), getRandomBool(), true); data.Add(salmon);
                        break;
                    case 5:
                        Trout trout = new Trout("Trout", data.Count, getRandomNumber(300), getRandomNumber(3000), getRandomBool(), true); data.Add(trout);
                        break;
                    case 6:
                        Shark shark = new Shark("Shark", data.Count, getRandomNumber(1000), getRandomNumber(10000), getRandomBool(), true); data.Add(shark);
                        break;
                    default:
                        Console.WriteLine("You have not written a valid input!");
                        break;
                }
                
            }
        }
        /// <summary>
        /// Promts user to choose a filter then uses GetFilterAmount method to get amount
        /// return true or false depending on filter choosen
        /// </summary>
        /// <param name="fish"></param>
        /// <returns></returns>
        public static bool Filter(bool fish)
        {
            if (fish) Console.WriteLine("Filter: \n '1' = weight \n '2' lenght");
            else Console.WriteLine("Filter: \n '1' = weight \n '2' wingspan");
            bool redo;
            do
            {
                redo = false;
                Console.Write("Command: ");
                var filter = Console.ReadLine();
                if (filter == "1") { GetFilterAmount(true); return true; }
                else if (filter == "2") { GetFilterAmount(false); return false; }
                else { redo = true; Console.WriteLine("Not a valid choice try again"); }
            } while (redo);
            return false;
        }
        /// <summary>
        /// Promts user for an amount to filter on, and updates values present in Program.cs
        /// </summary>
        /// <param name="weight"></param>
        public static void GetFilterAmount(bool weight)
        {
            bool redo;
            Program.FilterWeight = 0;
            Program.FilterLenght = 0;
            do
            {
                redo = false;
                if (weight)
                {
                    Console.Write("Have to be minimum amount of kg (i.e '100'): ");
                    var cm = Console.ReadLine();
                    if (HelperClass.IsDigitsOnly(cm)) Program.FilterWeight = int.Parse(cm) * 10;
                    else { Console.WriteLine("Wrong input! It will only accept numbers"); redo = true; }
                }
                else
                {
                    Console.Write("Have to be minimum amount of cm (i.e '100'): ");
                    var cm = Console.ReadLine();
                    if (HelperClass.IsDigitsOnly(cm)) Program.FilterLenght = int.Parse(cm) * 10;
                    else { Console.WriteLine("Wrong input! It will only accept numbers"); redo = true; }
                }
            } while (redo);
        }

        /// <summary>
        /// generates a random number within the max limit of "limit"
        /// </summary>
        /// <param name="limit"></param>
        /// <returns></returns>
        public static int getRandomNumber(int limit)
        {
            int number = RandomNumberGenerator.GetInt32(limit);
            return number;
        }
        /// <summary>
        /// Generates a random bool by creating a random number from 0 to 10 (could also use 1)
        /// </summary>
        /// <returns></returns>
        public static bool getRandomBool()
        {
            int number = RandomNumberGenerator.GetInt32(10);
            if (number < 5) return false;
            else return true;
        }

        /// <summary>
        /// Validates that string only have numbers in it. 
        /// </summary>
        /// <param name="str">user input as string</param>
        /// <returns>false if there is non numbers present in input string</returns>
        public static bool IsDigitsOnly(string str)
        {
            foreach (char c in str)
            {
                if (c < '0' || c > '9')
                    return false;
            }
            return true;
        }
    }
}
